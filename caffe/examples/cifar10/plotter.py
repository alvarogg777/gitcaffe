#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
python plotter.py
@brief Plot training and validation curves.
@author Roberto Valle
"""

import sys
import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot_curves(output_file, train_log, test_log):
    # Read training iterations
    with open(output_file, 'r') as ofs:
        for line in ofs:
            if re.match("(.*)max_iter(.*)", line):
                max_iter = int(line.split(' ')[-1])
    ofs.close()
    max_loss = np.max(test_log["loss"])

    # Draw plots
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.xlim(0, max_iter)
    plt.ylim(0, max_loss)
    plt.plot(train_log["NumIters"], train_log["loss"], linewidth=3, label='Train')
    plt.plot(test_log["NumIters"], test_log["loss"], linewidth=3, label='Validation')
    # Draw training epoch lines
    print("Best Model = " + str(np.argmin(test_log["loss"])))
    print("Min Loss = " + str(np.min(test_log["loss"])))
    ax.tick_params(axis="x", labelsize=13)
    ax.tick_params(axis="y", labelsize=13)
    ax.set_xlabel("Iteration", fontsize=15)
    ax.set_ylabel("Loss", fontsize=15)
    plt.grid("on", linestyle="--", linewidth=0.5)
    plt.legend(loc="upper right", fontsize=15)
    plt.show()
    plt.close()


def main():
    """
    This script plots the training and validation curves of the net
    """
    if len(sys.argv) == 3:
        print('Started ...')
        # Read input parameters
        # example: example.log caffe/python
        output_file = sys.argv[1]
        python_path = sys.argv[2]
        print('Generate train and test log files ...')
        os.system(python_path + "/../tools/extra/parse_log.py " + output_file + " .")
        train_log = pd.read_csv(output_file + '.train')
        os.remove(output_file + '.train')
        # Check if validation data exists
        test_log = pd.read_csv(output_file + '.test')
        os.remove(output_file + '.test')
        print('Generating curves ...')
        plot_curves(output_file, train_log, test_log)

    else:
        print('Usage: plotter.py file.log caffe/python')


if __name__ == '__main__':
    main()
