#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
python train_cifar10.py
@brief Train net using CIFAR10.
@author Roberto Valle
"""

import sys
import caffe


def main():
    """
    We need to indicate the solver mode, either CPU or GPU.
    Optionally, if we finetune instead of train from scratch, we indicate a caffemodel
    """
    if len(sys.argv) >= 3:
        print('Started ...')
        # Read input parameters
        # example: cifar10_mlp_solver.prototxt gpu [cifar10.caffemodel]
        model_solver = sys.argv[1]
        if sys.argv[2] == 'gpu' or sys.argv[2] == 'GPU':
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu()
        print('Running solver ...')
        solver = caffe.get_solver(model_solver)
        # Train from scratch vs Finetune from a previous net
        if len(sys.argv) == 4:
            model_weights = sys.argv[3]
            solver.net.copy_from(model_weights)
        solver.solve()

    else:
        print('Usage: train_cifar10.py caffe_solver mode [caffe_model]')


if __name__ == '__main__':
    main()
