#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
python test_cifar10.py
@brief Test net using CIFAR10.
@author Roberto Valle
"""

import sys
import caffe
import numpy as np
import lmdb


def main():
    """
    This script test an image over the caffemodel defined by the prototxt structure.
    We decide whether to use the GPU or the CPU to test the net.
    """
    if len(sys.argv) == 6:
        print('Started ...')
        # Read input parameters
        # example: cifar10.caffemodel cifar10_mlp_deploy.prototxt gpu cifar10_test_lmdb mean.binaryproto
        model_weights = sys.argv[1]
        model_deploy = sys.argv[2]
        if sys.argv[3] == "gpu" or sys.argv[3] == "GPU":
            caffe.set_mode_gpu()
        else:
            caffe.set_mode_cpu()
        lmdb_test = sys.argv[4]
        mean_file = sys.argv[5]
        # Extract mean from the mean image file
        mean_blobproto_new = caffe.proto.caffe_pb2.BlobProto()
        f = open(mean_file, 'rb')
        mean_blobproto_new.ParseFromString(f.read())
        mean_image = caffe.io.blobproto_to_array(mean_blobproto_new)
        f.close()
        # CNN reconstruction and loading the trained weights
        net = caffe.Net(model_deploy, model_weights, caffe.TEST)
        print('Predict images ...')
        count, correct = 0, 0
        lmdb_cursor = lmdb.open(lmdb_test).begin().cursor()
        for key, value in lmdb_cursor:
            datum = caffe.proto.caffe_pb2.Datum()
            datum.ParseFromString(value)
            label = int(datum.label)
            image = caffe.io.datum_to_array(datum)
            # import matplotlib.pyplot as plt
            # plt.imshow(np.transpose(image, (1,2,0)).astype(np.uint8))  # channels-rows-cols -> rows-cols-channels
            # plt.show()
            net.blobs['data'].data[...] = np.asarray([image]) - mean_image
            net.forward()
            prediction = net.blobs['ip3'].data[0]
            plabel = int(prediction.argmax(axis=0))
            count += 1
            correct += (1 if label == plabel else 0)
            #print("Accuracy: " + str(100. * correct / count))
        print(str(correct) + " out of " + str(count) + " were classified correctly")

    else:
        print('Usage: test_cifar10.py caffe_model caffe_deploy mode lmdb_test mean_file')


if __name__ == '__main__':
    main()
