import matplotlib.pyplot as plt
import h5py
import sys
import caffe
import matplotlib.pyplot as plt
import numpy as np
def main():
    #model_deploy,model_weights,caffe.TEST
    model_deploy,model_weights,filename=sys.argv[1:]
    #net = caffe.Net(model_deploy, model_weights)
    net = caffe.Net(str(model_deploy), str(model_weights),caffe.TEST)
    img_size = net.blobs['data'].data.shape[2]
    net.blobs['data'].reshape(1, 3, img_size, img_size)
    print('opennnnn')
    f = h5py.File(filename, 'r')
    labels = list(f['label'])
    data = list(f['data'])
    mae_by_image_yaw = []
    mae_by_image_pitch = []
    mae_by_image_roll = []
    print('Predict images ...')
    for i in range(0,500):
        net.blobs['data'].data[...] = data[i]
        net.forward()
        #prediction = net.blobs[next(reversed(net.blobs))].data
        prediction = net.blobs['3pose'].data
        #print(len(labels),'\n',len(prediction))
        mae_by_image_yaw.append(abs(labels[i][0] - prediction[0][0]))
        mae_by_image_pitch.append(abs(labels[i][1] - prediction[0][1]))
        mae_by_image_roll.append(abs(labels[i][2] - prediction[0][2]))
    #print(mae_by_image_yaw)
    print('Mean Absolute Error yaw:',np.mean(mae_by_image_yaw))
    #print(mae_by_image_pitch)
    print('Mean Absolute Error pitch:',np.mean(mae_by_image_pitch))
    #print(mae_by_image_roll)
    print('Mean Absolute Error roll:',np.mean(mae_by_image_roll))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.xlim(0, 30)
    plt.ylim(0, 1)
    num_imgs_yaw, base = np.histogram(mae_by_image_yaw, bins=1000)
    cumulative_yaw = [x/float(len(mae_by_image_yaw)) for x in np.cumsum(num_imgs_yaw)]
    num_imgs_pitch, base = np.histogram(mae_by_image_pitch, bins=1000)
    cumulative_pitch = [x/float(len(mae_by_image_yaw)) for x in np.cumsum(num_imgs_pitch)]
    num_imgs_roll, base = np.histogram(mae_by_image_roll, bins=1000)
    cumulative_roll = [x/float(len(mae_by_image_roll)) for x in np.cumsum(num_imgs_roll)]
    plt.plot(base[0:1000], cumulative_yaw[0:1000], 'r-', label="Yaw", linewidth=3)
    plt.plot(base[0:1000], cumulative_pitch[0:1000], 'g-', label="Pitch", linewidth=3)
    plt.plot(base[0:1000], cumulative_roll[0:1000], 'b-', label="Roll", linewidth=3)
    ax.tick_params(axis='x', labelsize=13)
    ax.tick_params(axis='y', labelsize=13)
    ax.set_xlabel('Absolute head-pose error', fontsize=15)
    ax.set_ylabel('Images proportion', fontsize=15)
    plt.grid('on', linestyle='--', linewidth=0.5)
    plt.legend(loc='lower right', fontsize=15)
    plt.show()
    plt.close()

if __name__=="__main__":
    if len(sys.argv)!=4:
        print("Error: this script takes 3 arguments as input: \n\tdeploy.prototxt,model/pose_iter_22400.caffemodel.h5,test_file.h5")
        exit(1)
    main()
