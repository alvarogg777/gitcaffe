export PYTHONPATH="/home/alvaro/caffe/python"
export PYTHONPATH="${PYTHONPATH}:/home/alvaro/models/resnet-cifar10-caffe-master"
# ${PYTHONPATH}:
set -x

#GPUs=$1
NET=$1
# EXTRA_ARGS=${array[@]:3:$len}
# EXTRA_ARGS_SLUG=${EXTRA_ARGS// /_}
OTHER=$2
EXTRA_ARGS_SLUG=$2
w=$3
p=$4

solver=${NET}/${OTHER}solver.prototxt
LOG=${NET}"/logs/${NET}_${EXTRA_ARGS_SLUG}_`date +'%Y-%m-%d_%H-%M-%S'`.log"
exec &> >(tee -a "$LOG")
echo Logging output to "$LOG"

time /home/alvaro/caffe/build/tools/caffe train  \
    -solver ${solver} \
    -sighup_effect stop ${w} ${p}
#    ${EXTRA_ARGS}

set +x

echo -e "\a"
